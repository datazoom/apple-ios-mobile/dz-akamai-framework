//
//  DZAkamaiCollector.h
//  DZAkamaiCollector
//
//  Created by benedict placid on 05/03/19.
//  Copyright © 2019 benedict placid. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DZAkamaiCollector.
FOUNDATION_EXPORT double DZAkamaiCollectorVersionNumber;

//! Project version string for DZAkamaiCollector.
FOUNDATION_EXPORT const unsigned char DZAkamaiCollectorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DZAkamaiCollector/PublicHeader.h>
